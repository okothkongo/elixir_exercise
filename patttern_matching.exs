a = [1,2,3] #matching
a = [4]#matching
4 =a #not matching,assigning variable to literal
[a, b] = [ 1, 2, 3 ] #not matching unmatching list elements
a = [ [ 1, 2, 3 ] ] #matching
[a] = [ [ 1, 2, 3 ] ] #matching
[[a]] = [ [ 1, 2, 3 ] ] #not matching

#exercise 2
[a, b, a ] = [ 1, 2, 3 ] #not matching variable assigned to more different literal
[ a, b, a ] = [ 1, 1, 2 ] #not matching variable assigned to more different literal
[ a, b, a ] = [ 1, 2, 1 ] #will match
#exercise3
[ a, b, a ] = [ 1, 2, 3 ] #will not match
[ a, b, a ] = [ 1, 1, 2 ] #will not match
a=1 #will match
^a = 2  # will not match pin operator is bound to one
^a = 1 #will match
^a = 2 - a #will match
